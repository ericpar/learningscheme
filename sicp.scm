;;; Exercices faits dans le cadre de la lecture du livre "Structure
;;; and Implementation of Computer Programs" (SICP) tel que disponible
;;; sur : https://github.com/sarabander/sicp-pdf
;;;
;;; Les exemples sont ex�cut�s sur Gambit v4.7.2 compil� sur Debian Wheezy
;;; 32 bits OU sur RHEL 6.5 avec SVM (Ad Opt).
;;;

;;; 1 - Building Abstractions with Procedures

(define nil '())
(define (average a b) (/ (+ a b) 2))
(define (identity x) x)
(define (inc n) (+ 1 n ))
(define (double n) (+ n n))
(define (square x) (* x x))
(define (cube x) (* x x x))

(define (sqrt x)
  (define (good-enough? guess)
    (< (abs (- (square guess) x)) 0.0001))
  (define (improve guess)
    (average guess (/ x guess)))
  (define (sqrt-iter guess)
    (if (good-enough? guess)
        guess
        (sqrt-iter (improve guess))))
  (sqrt-iter 1.0))
(sqrt 2.0) ; 1.4142156862745097
(sqrt 9)   ; 3.000000001396984
(square (sqrt 1000))  ; 1000.0000000000343

;; Exercice 1.7
(define (sqrt x)
  (let ((previous-guess #f)
        (epsilon 0.0000001))
    (define (close-enough? new-guess) ; the improvement is computed
                                      ; relatively to the guess
                                      ; (old-guess)
      (if (not previous-guess)
          #f
          (< (/ (abs (- new-guess previous-guess)) previous-guess) epsilon)))
    (define (improve old-guess)
      (set! previous-guess old-guess)
      ;(print "old-guess = " old-guess)
      ;(print "\n")
      (average previous-guess (/ x previous-guess)))
    (define (sqrt-iter guess)
      ;(print "guess = " guess)
      ;(print "\n")
      (if (close-enough? guess)
          guess
          (sqrt-iter (improve guess))))
    (sqrt-iter 1.0)))

(sqrt 2.0)
; old-guess = 1.4142135623746899
; guess = 1.414213562373095

(sqrt 14.0) ; 3.7416573867739458
(sqrt 31)   ; 5.567764362830022

;; Exercice 1.10

(define (A x y)
  (cond ((= y 0) 0)
        ((= x 0) (* 2 y))
        ((= y 1) 2)
        (else (A (- x 1) (A x (- y 1))))))

(A 1 10)  ; svm> 1024
(A 2 4)   ; svm> 65536
(A 3 3)   ; 

;; Fibonacci numbers (iterative way)
(define (fib n)
  (define (fib-iter a b count)
    (if (= count 0)
        b
        (fib-iter (+ a b) a (- count 1))))
  (fib-iter 1 0 n))

(fib 35)
;9227465
(fib 80)
;23416728348467685


;; The change problem
(define (count-change amount) (cc amount 5))
(define (cc amount kinds-of-coins)
  (cond ((= amount 0) 1)
        ((or (< amount 0) (= kinds-of-coins 0)) 0)
        (else (+ (cc amount
                     (- kinds-of-coins 1))
                 (cc (- amount
                         (first-denomination
                          kinds-of-coins))
                     kinds-of-coins)))))
(define (first-denomination kinds-of-coins)
  (cond ((= kinds-of-coins 1) 1)
        ((= kinds-of-coins 2) 5)
        ((= kinds-of-coins 3) 10)
        ((= kinds-of-coins 4) 25)
        ((= kinds-of-coins 5) 50)))

(count-change 100)
;292
(count-change 97)
;252

;; Exercice 1.12 - le triangle de Pascal
; Soit P(a,b) la fonction qui calcule le triangle de Pascal pour a et
; b, a �tant la profondeur dans le triangle et b la position du nombre
; sur la ligne, en partant de la gauche.

; Par d�finition, on a : P(n,1) = P(n,n) = 1, pour tout n >= 1
;
; De plus, on a que P(a,b) = P(a-1, b) + P(a-1, b+1)

(define (Pascal a b)
  (cond
   ((> b a) 0)
   ((or (= a 1) (= b a) (= b 1)) 1)
   (else  (+ (Pascal (- a 1) b) (Pascal (- a 1) (- b 1))))))

(Pascal 1 1)

(Pascal 2 1)
(Pascal 2 2)

(Pascal 3 1)
(Pascal 3 2)
(Pascal 3 3)

(Pascal 4 1)
(Pascal 4 2)
(Pascal 4 3)
(Pascal 4 4)

(Pascal 5 1)
(Pascal 5 2)
(Pascal 5 3)
(Pascal 5 4)
(Pascal 5 5)

(Pascal 3 4)

; REMARQUE : il y a un moyen de calculer le nombre de Pascal de
; mani�re dynamique...

;; 1.2.4 Fast exponentiation
(define (fast-expt b n)
  (cond ((= n 0) 1)
        ((even? n) (square (fast-expt b (/ n 2))))
        (else (* b (fast-expt b (- n 1))))))

(fast-expt 4 17)
; 17179869184


;; 1.2.3 - Greatest Common Divisor

(define (gcd a b)
  (if (= b 0)
      a
      (gcd b (remainder a b))))


;; Section 1.3.1 -- sommations

(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a)
         (sum term (next a) next b))))

(define (sum-cubes a b)
  (sum cube a inc b))
(sum-cubes 1 10)     ; 3025

(define (sum-integers a b)
  (sum identity a inc b))
(sum-integers 1 10)  ; 55

(define (integral f a b dx)
  (define (add-dx x)
    (+ x dx))
  (* (sum f (+ a (/ dx 2.0)) add-dx b)
     dx))

(define (pi-sum a b)
  (define (pi-term x)
    (/ 1.0 (* x (+ x 2))))
  (define (pi-next x)
    (+ x 4))
  (sum pi-term a pi-next b))
(* 8 (pi-sum 1 1000))        ; 3.1395926555897828

(define (integral f a b dx)
  (define (add-dx x)
    (+ x dx))
  (* (sum f (+ a (/ dx 2.0)) add-dx b)
          dx))

(integral cube 0 1 0.001)    ; 0.24999987500000101
(integral cube 0 1 0.0001)   ; 0.24999999874993412
(integral cube 0 1 0.000001) ; 0.25000000000144468

; � FAIRE : Exercice 1.30 -- modifier la fonction "sum" afin qu'elle soit
; plut�t it�rative que r�cursive.

(define (sum-1.30 term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (inc a) (+ result (term a)))))
    (iter a 0))

(sum-1.30 identity 1 inc 10)   ; 55
(sum-1.30 cube 1 inc 10)       ; 3025

; � FAIRE : Exercice 1.31 -- d�finir une fonction Scheme qui calcule le
; produit (au lieu de la somme. Utiliser cette m�thode pour approximer la
; valeur Pi/4 = (2*4*4*6*6*8...) / (3*3*5*5*7*7...)

(define (product term a next b)
  (if (> a b)
      1
      (* (term a)
         (product term (next a) next b))))

(define (pi-1.32 n)
  (define (double n) (* n 2))
  (define (prod-term a)
    (let ((q (+ 1 (double a))))
      (* (/ (double a) q)
         (/ (double (+ a 1)) q))))
  (product prod-term 1 inc n))

(pi-1.32 10)    ; Gambit => 68719476736/85530896451
(pi-1.32 100)
(pi-1.32 1000)

;; Exemple 1.2.6 - tester la primalit�

(define (smallest-divisor n) (find-divisor n 2))
(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))
(define (divides? a b) (= (remainder b a) 0))

(define (prime? n)
  (= n (smallest-divisor n)))

(prime? 11)        ; #t
(prime? 1234569)   ; #f

;; petit th�or�me de Fermat

(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder (square (expmod base (/ exp 2) m))
                    m))
        (else
         (remainder (* base (expmod base (- exp 1) m))
                    m))))

(filter prime? (enumerate-interval 1 3000))


; � FAIRE : Exercice 1.32

(define (accumulate combiner neutral-value term a next b)
  (if (> a b)
      neutral-value
      (combiner (term a)
                (accumulate combiner neutral-value term (next a) next b))))

(define (sum-1.32 term a next b)
    (accumulate + 0 term a next b))
(sum-1.32 identity 1 inc 10)    ; 55

(define (product-1.32 term a next b)
  (accumulate * 1 term a next b))
(product-1.32 identity 1 inc 3) ; 6

; � FAIRE : Exercice 1.33

(define (filtered-accumulate combiner filter neutral-value term a next b)
  (if (> a b)
      neutral-value
      (combiner (if (not (filter a))
                    neutral-value
                    (term a))
                (filtered-accumulate combiner filter neutral-value term (next a) next b))))

(define (sum-even a b)
  (filtered-accumulate + even? 0 identity a inc b))

(sum-even 1 5)   ; 6


;; Section 1.3.3 -- m�thode de bisection (half-inverval method)
(define (average x y)
  (/ (+ x y) 2))
(define (close-enough? x y) (< (abs (- x y)) 0.0001))
(define (search f neg-point pos-point)
  (let ((midpoint (average neg-point pos-point)))
    (if (close-enough? neg-point pos-point)
        midpoint
        (let ((test-value (f midpoint)))
          (cond ((positive? test-value)
                 (search f neg-point midpoint))
                ((negative? test-value)
                 (search f midpoint pos-point))
                (else midpoint))))))

(define (half-interval-method f a b)
  (let ((a-value (f a))
        (b-value (f b)))
    (cond ((and (negative? a-value) (positive? b-value))
           (search f a b))
          ((and (negative? b-value) (positive? a-value))
           (search f b a))
          (else
           (error "Values are not of opposite sign" a b)))))

(half-interval-method sin 2.0 4.0)   ; 3.14111328125
(half-interval-method (lambda (x) (- (* x x x) (* 2 x) 3))
                      1.0
                      2.0)           ; 1.893280029296875

; M�thode de point fixe
(define tolerance 0.00001)
(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
;      (print "guess = " next)
;      (newline)
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

(fixed-point cos 1.0)
    ; .7390822985224024
(fixed-point (lambda (y) (+ (sin y) (cos y)))
             1.0)
    ; 1.2587315962971173

; Revisiter l'exercice de calcul d'une racine carr�e ?!?

(define (sqrt2 x)
  (fixed-point (lambda (y) (average y (/ x y))) 1.0))
(sqrt2 2.0)              ; 1.4142135623746899

(define (average-damp f)
  (lambda (x) (average x (f x))))
(define (sqrt x)
  (fixed-point (average-damp (lambda (y) (/ x y)))
               1.0))
(sqrt 10)

;; Section 1.3.4

(define dx 0.000001)
(define (deriv g)
  (lambda (x) (/ (- (g (+ x dx)) (g x)) dx)))
((deriv cube) 5)   ; 75.000015016257748

(define (newton-transform g)
  (lambda (x) (- x (/ (g x) ((deriv g) x)))))
(define (newtons-method g guess)
  (fixed-point (newton-transform g) guess))
(define (sqrt x)
  (newtons-method
   (lambda (y) (- (square y) x)) 1.0))
(sqrt 10)   ; 3.16227766016838
(sqrt 2)    ; 1.4142135623754424

; IDEES : Calculer la moyenne d'un nombre arbitraire de nombres : utiliser
; un nombre arbitraire d'arguments

;;; 2 - Building Abstractions with Data

;; Nombres rationels

; Exercice 2.1
(define (make-rat n d)
  (letrec ((denominator (abs d))
           (g (gcd (abs n) (abs d)))
           (numerator (if (or (negative? n) (negative? d))
                          (* -1 (abs n))
                          n)))
    (cons (/ numerator g) (/ denominator g))))
(define numer car)
(define denom cdr)
(define (add-rat x y)
  (make-rat (+ (* (numer x) (denom y))
               (* (numer y) (denom x)))
            (* (denom x) (denom y))))
(define (sub-rat x y)
  (make-rat (- (* (numer x) (denom y))
               (* (numer y) (denom x)))
            (* (denom x) (denom y))))
(define (mul-rat x y)
  (make-rat (* (numer x) (numer y))
            (* (denom x) (denom y))))
(define (div-rat x y)
  (make-rat (* (numer x) (denom y))
            (* (denom x) (numer y))))
(define (equal-rat? x y)
  (= (* (numer x) (denom y))
     (* (numer y) (denom x))))
(define (print-rat x)
       (newline)
       (display (numer x))
       (display "/")
       (display (denom x)))
(define one-half (make-rat 1 2))
(define one-third (make-rat 1 3))
(define one-fourth (make-rat 1 4))

(print-rat (add-rat one-half one-third))
(print-rat (sub-rat one-half one-third))
(print-rat (add-rat one-third one-third))
(print-rat (mul-rat one-half one-third))
(print-rat (div-rat one-half one-third))
(print-rat (add-rat (make-rat -2 16) (make-rat -2 8)))  ; 3/2
(print-rat (div-rat one-half one-third))

(print-rat (mul-rat (make-rat -2 16) (make-rat -2 8)))  ; 3/2


; Exercice 2.2

; point is a pair of integers (x, y)
(define make-point cons)
(define x-point car)
(define y-point cdr)
(define (print-point pt)
  (newline)
  (display "(")
  (display (x-point pt))
  (display ",")
  (display (y-point pt))
  (display ")"))

(print-point (make-point 3 9))

; segment is a pair of points

(define make-segment cons)
(define end-segment cdr)
(define start-segment car)
(define (print-segment seg)
  (newline)
  (display "from ")
  (print-point (start-segment seg))
  (display " to ")
  (print-point (end-segment seg)))

(cdr (make-segment (make-point 3 4) (make-point 5 2)))
(print-point (car (make-segment (make-point 3 4) (make-point 5 2))))
(print-point (cdr (make-segment (make-point 3 4) (make-point 5 2))))
(make-segment (make-point 3 4) (make-point 5 2))
(print-segment (make-segment (make-point 3 4) (make-point 5 2)))


; Exercice 2.17
(define (last-pair L)
  (cond ((null? L) L)
        ((null? (cdr L)) (car L))
        (else (last-pair (cdr L)))))

(last-pair (list 23 72 149 34))

; Exercice 2.18
(define (reverse-ex X)
  (if (null? X)
      '()
      (append (reverse-ex (cdr X)) (list (car X)))))
(reverse-ex (list 2 3 7 9))

; Exercices 2.20

(define (same-parity . arguments*)
  (define (same-parity-iter parity args* selected)
    (if (null? args*)
        (reverse selected)
        (if (= parity (modulo (car args*) 2))
            (same-parity-iter parity (cdr args*) (cons (car args*) selected))
            (same-parity-iter parity (cdr args*) selected))))
  (same-parity-iter (modulo (car arguments*) 2) (cdr arguments*) (list (car arguments*))))
(same-parity 1 2 3 4 5 6)
(same-parity 2 3 4 5 6 7 8 9 20 22)

(define (count-leaves x)
  (cond ((null? x) 0)
        ((not (pair? x)) 1)
        (else (+ (count-leaves (car x))
                 (count-leaves (cdr x))))))
(count-leaves (list 1 (list 2 (list 3 4))))  ; 4

; Exercice 2.27 -- deep-reverse : fait le reverse de chacune des
; sous-listes. Par exemple : (deep-reverse (list (list 1 2) (list 3
; 4))) ((4 3) (2 1))

; Rappel :
(define (reverse-ex X)
  (if (null? X)
      '()
      (append (reverse-ex (cdr X)) (list (car X)))))
(reverse-ex (list (list 2 3) (list 7 9)))

(define (deep-reverse items*)
  (if (null? items*)
      '()
      (append (deep-reverse (cdr items*)) (list (reverse-ex (car items*))))))
(deep-reverse (list (list 2 3) (list 7 9)))

; Exercice 2.28 -- fringe : retourne la liste des feuilles telles que
; pr�sentes de gauche � droite dans l'arbre.

(define (fringe tree)
  (if (null? tree)
      '()
      (if (pair? tree)
          (append (fringe (car tree)) (fringe (cdr tree)))
          (list tree))))
(fringe (list (list 2 3) (list 7 9)))  ; (2 3 7 9)

; Exercice 2.29 -- mobiles binaires

(define (make-mobile left right)
  (list left right))
(define (make-branch Length Structure)
  (list Length Structure))

(define (left-branch mobile)
  (car mobile))
(define (right-branch mobile)
  (cadr mobile))
(define (branch-length branch)
  (car branch))
(define (branch-structure branch)
  (cadr branch))
(define (branch-weight branch)
  (if (pair? (branch-structure branch))
      (total-weight (branch-structure branch))
      (branch-structure branch)))

(define (total-weight mobile)
  (+ (branch-weight (left-branch mobile))
     (branch-weight (right-branch mobile))))

(define (branch-torque branch)
  (* (branch-length branch)
     (branch-weight branch)))

(define (branch-balanced? branch)
  (if (pair? (branch-structure branch))
      (balanced? (branch-structure branch))
      #t))

(define (balanced? mobile)
  (and (= (branch-torque (right-branch mobile))
          (branch-torque (left-branch mobile)))
       (branch-balanced? (right-branch mobile))
       (branch-balanced? (right-branch mobile))))

(define A (make-mobile (make-branch 5 2)
                       (make-branch 5 2)))
(define B
  (make-mobile (make-branch 1 (make-mobile (make-branch 2 3)
                                           (make-branch 4 5)))
               (make-branch 7 8)))

(total-weight A)
(balanced? A)

(left-branch B)
(right-branch (left-branch B))
(right-branch B)
(left-branch (right-branch B))
(branch-length (right-branch B))
(branch-structure (right-branch B))
(branch-length (left-branch B))
(branch-structure (left-branch B))
(total-weight B)
(balanced? B)

(define C (make-mobile (make-branch 10 (make-mobile (make-branch 2 3)
                                                    (make-branch 2 3)))
                       (make-branch 12 5)))
(balanced? C)


; d) si on change 'make-mobile' et 'make-branch', on doit aussi changer
; 'branch-structure' et 'right-branch'. Ceci est parfaitement raisonnable
; car on a chang� la structure des branches. On doit donc changer la
; mani�re dont on extrait les informations dans les branches (ou
; structures).
;
(define (make-mobile left right)
  (cons left right))
(define (make-branch Length Structure)
  (cons Length Structure))

(define branch-structure cdr)
(define right-branch cdr)

(define C (make-mobile (make-branch 10 (make-mobile (make-branch 2 3)
                                                    (make-branch 2 3)))
                       (make-branch 12 5)))
(balanced? C)    ; #t

; Exercice 2.30

(define F (list 1 (list 2 (list 3 4) 5) (list 6 7)))

(define (scale-tree tree factor)
  (cond ((null? tree) '())
        ((not (pair? tree)) (* tree factor))
        (else (cons (scale-tree (car tree) factor)
                    (scale-tree (cdr tree) factor)))))
(scale-tree F 6)

(define (scale-tree-map tree factor)
  (map (lambda (sub-tree)
         (if (pair? sub-tree)
             (scale-tree-map sub-tree factor)
             (* sub-tree factor)))
       tree))
(scale-tree-map F 6)

(define (square-tree tree)
  (cond ((null? tree) '())
        ((not (pair? tree)) (* tree tree))
        (else (cons (square-tree (car tree))
                    (square-tree (cdr tree))))))
(square-tree F)

(define (square-tree-map tree)
  (map (lambda (sub-tree)
         (if (pair? sub-tree)
             (square-tree-map sub-tree)
             (* sub-tree sub-tree)))
       tree))
(square-tree-map F)

(define (subsets s)
  (if (null? s)
      (list '())
      (let ((rest (subsets (cdr s))))
        (append rest (map (lambda (x) (cons (car s) x))
                          rest)))))
(subsets (list 1 2 3))

;; Section 2.2.3 -- Sequences as conventional interfaces

(define (filter predicate sequence)
  (cond ((null? sequence) '())
        ((predicate (car sequence))
         (cons (car sequence)
               (filter predicate (cdr sequence))))
        (else (filter predicate (cdr sequence)))))

(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (enumerate-interval low high)
  (if (> low high)
      '()
      (cons low (enumerate-interval (+ low 1) high))))

(define (enumerate-tree tree)
  (cond ((null? tree) '())
        ((not (pair? tree)) (list tree))
        (else (append (enumerate-tree (car tree))
                      (enumerate-tree (cdr tree))))))

(define (sum-odd-squares tree)
  (accumulate
   + 0 (map square (filter odd? (enumerate-tree tree)))))

(define (even-fibs n)
  (accumulate
   cons
   '()
   (filter even? (map fib (enumerate-interval 0 n)))))
(even-fibs 10)

(define (list-fib-squares n)
  (accumulate
   cons
   '()
   (map square (map fib (enumerate-interval 0 n)))))
(list-fib-squares 15)
; (0 1 1 4 9 25 64 169 441 1156 3025 7921 20736 54289 142129 372100)

(define (product-of-squares-of-odd-elements sequence)
  (accumulate * 1 (map square (filter odd? sequence))))
(product-of-squares-of-odd-elements (list 1 2 3 4 5)) ; 225


; Exercice 2.33

(define (map-ex p sequence)
  (accumulate (lambda (x y) (cons (p x) y)) '() sequence))
(map-ex inc (list 2 4 6))
(map-ex square (list 2 4 6))
(map-ex double (list 2 4 6))

(define (append-ex seq1 seq2)
  (accumulate cons seq2 seq1))
(append-ex (list 1 2 3) (list 4 5 6))

(define (length-ex sequence)
  (accumulate (lambda (x y) (+ 1 y)) 0 sequence))
(length-ex (list 21 42 63))   ; 3

;; Exercice 2.34

(define (horner-eval x coefficient-sequence)
  (accumulate (lambda (this-coeff higher-terms)
                (+ (* x higher-terms) this-coeff))
              0
              coefficient-sequence))

(horner-eval 2 (list 1 3))         ;  7
(horner-eval 2 (list 1 3 0 5))     ; 47
(horner-eval 2 (list 1 3 0 5 0 1)) ; 79

;; Exercice 2.35

(define (count-leaves-acum t)
  (accumulate + 0 (map (lambda (x) 1) (enumerate-tree t))))
 
(define T (list 1 (list 8 9) (list 2 (list 3 4) 5) (list 6 7)))
(enumerate-tree T)
(count-leaves-acum T)   ; 9

;; Exercice 2.36

(define (accumulate-n op init seqs)
  (if (null? (car seqs))
      '()
      (cons (accumulate op init (map car seqs))
            (accumulate-n op init (map cdr seqs)))))
(accumulate-n + 0 (list (list 1 2 3) (list 4 5 6) (list 7 8 9)))

;; Exercice 2.37

(define m (list (list 1 2 3 4) (list 4 5 6 6) (list 6 7 8 9)))

(define (dot-product v w)
  ; The arguments are vectors in list representation
  (accumulate + 0 (map * v w)))

(dot-product '(1 1 1) '(4 5 6))  ; 15

(define (matrix-*-vector m v)
  (map (lambda (row) (dot-product row v)) m))
(matrix-*-vector m '(4 3 2 1))   ; (20 49 70)

(define (transpose mat)
  (accumulate-n cons '() mat))
(transpose m)   ; ((1 4 6) (2 5 7) (3 6 8) (4 6 9))

(define (matrix-*-matrix m n)
  (let ((cols (transpose n)))
    (map (lambda (row) (matrix-*-vector cols row)) m)))

(matrix-*-matrix
 '((1 0 0) (0 1 0) (0 0 1))
 '((4 5 7) (5 8 9) (9 11 14)))


;; Nested Mappings

(accumulate
 append
 '()
 (map (lambda (i)
        (map (lambda (j) (list j i))
             (enumerate-interval 1 (- i 1))))
      (enumerate-interval 1 5)))

(define (flatmap proc seq)
  (accumulate append nil (map proc seq)))

(define (prime-sum? pair)
  (prime? (+ (car pair) (cadr pair))))
(prime-sum? '(3 8))

(define (make-pair-sum pair)
  (list (car pair) (cadr pair) (+ (car pair) (cadr pair))))

(flatmap
 (lambda (i)
   (map (lambda (j) (list i j))
        (enumerate-interval 1 (- i 1))))
 (enumerate-interval 1 5))

(accumulate append '()
            (map
             (lambda (i)
               (map (lambda (j) (list i j))
                    (enumerate-interval 1 (- i 1))))
             (enumerate-interval 1 5)))

(define (prime-sum-pairs n)
  (map make-pair-sum
       (filter prime-sum? (flatmap
                           (lambda (i)
                             (map (lambda (j) (list i j))
                                  (enumerate-interval 1 (- i 1))))
                           (enumerate-interval 1 n)))))

(prime-sum-pairs 10)

;; �num�ration des permutations d'un ensemble

(define (permutations s)
  (if (null? s)
      (list '())
      (flatmap (lambda (x)
                 (map (lambda (p) (cons x p))
                      (permutations (remove x s))))
               s)))

(define (remove item sequence)
  (filter (lambda (x) (not (equal? x item)))
          sequence))

(permutations (enumerate-interval 1 3))
(permutations '('pomme 'orange 'raisin))

;; Exercice 2.40

;; Exercice 2.41

;; Exercice 2.42 - probl�me des reines sur un �chiquier

(define (queens board-size)
  (define (queen-cols k)
    (if (= k 0)
        (list empty-board)
        (filter
         (lambda (positions) (safe? k positions))
         (flatmap
          (lambda (rest-of-queens)
            (map (lambda (new-row)
                   (adjoin-position
                    new-row k rest-of-queens))
                 (enumerate-interval 1 board-size)))
          (queen-cols (- k 1))))))
  (queen-cols board-size))

(define (make-position row col)
  (cons row col))
(define (position-row position)
  (car position))
(define (position-col position)
  (cdr position))

(define empty-board '())
(define (adjoin-position row col positions)
  (append positions (list (make-position row col))))

(define (safe? col positions)
  (let ((kth-queen (list-ref positions (- col 1)))
        (other-queens (filter (lambda (q)
                                (not (= col (position-col q))))
                              positions)))
    (define (attacks? q1 q2)
      (or (= (position-row q1) (position-row q2))
          (= (abs (- (position-row q1) (position-row q2)))
             (abs (- (position-col q1) (position-col q2))))))
    (define (iter q board)
      (or (null? board)
          (and (not (attacks? q (car board)))
               (iter q (cdr board)))))
    (iter kth-queen other-queens)))

(define (display-queens solutions)
  (if (null? solutions)
      '()
      (and (display (car solutions))
           (newline)
           (display-queens (cdr solutions)))))

(require "svmutil/clock")
(cpu-time 2 (queens 4))
(cpu-time 2 (queens 6))
(cpu-time 2 (queens 8))
(display-queens (queens 4))


;; 3 - Symbolic Data

;; D�rivation

(define (variable? x) (symbol? x))
(define (same-variable? v1 v2)
  (and (variable? v1) (variable? v2) (eq? v1 v2)))
(define (make-sum a1 a2)
  (list '+ a1 a2))
(define (make-product m1 m2)
  (list '* m1 m2))
(define (sum? x)
  (and (pair? x)
       (eq? (car x) '+)))
(define (addend s)
  (cadr s))
(define (augend s)
  (caddr s))
(define (product? x)
  (and (pair? x)
       (eq? (car x) '*)))
(define (multiplier p)
  (cadr p))
(define (multiplicand p)
  (caddr p))
(define (=number? exp num)
  (and (number? exp) (= exp num)))

(define (deriv exp var)
  (cond ((number? exp) 0)
        ((variable? exp) (if (same-variable? exp var) 1 0))
        ((sum? exp) (make-sum (deriv (addend exp) var)
                              (deriv (augend exp) var)))
        ((product? exp)
         (make-sum
          (make-product (multiplier exp)
                        (deriv (multiplicand exp) var))
          (make-product (deriv (multiplier exp) var)
                        (multiplicand exp))))
        (else
         (error "unknown expression type: DERIV" exp))))
(deriv '(+ x 3) 'x)
(deriv '(* x y) 'x)
(deriv '(* (* x y) (+ x 3)) 'x)

(define (make-sum a1 a2)
  (cond ((=number? a1 0) a2)
        ((=number? a2 0) a1)
        ((and (number? a1) (number? a2))
         (+ a1 a2))
        (else (list '+ a1 a2))))

(define (=number? exp num)
  (and (number? exp) (= exp num)))

(define (make-product m1 m2)
  (cond ((or (=number? m1 0) (=number? m2 0)) 0)
        ((=number? m1 1) m2)
        ((=number? m2 1) m1)
        ((and (number? m1) (number? m2)) (* m1 m2))
        (else (list '* m1 m2))))

(deriv '(+ x 3) 'x)             ; 1
(deriv '(* x y) 'x)             ; y
(deriv '(* (* x y) (+ x 3)) 'x) ; (+ (* x y) (* y (+ x 3)))

;; 2.3.3 - Ensembles

(define (element-of-set? x set)
  (cond ((null? set) #f)
         ((equal? x (car set)) #t)
         (else (element-of-set? x (cdr set)))))
(element-of-set? 'a '(a b c d))    ; #t
(element-of-set? 'a (list 1 2 3))  ; #f

(define (adjoin x set)
  (if (element-of-set? x set)
      set
      (cons x set)))
(adjoin 'a '(a b c))   ; (a b c)
(adjoin 'f '(a b c))   ; (f a b c)

(define (intersection-set set1 set2)
  (cond ((or (null? set1) (null? set2)) '())
        ((element-of-set? (car set1) set2)
         (cons (car set1) (intersection-set (cdr set1) set2)))
        (else (intersection-set (cdr set1) set2))))
(intersection-set '(a b c d e) '(a p q r e f))   ; (a e)

(define (union-set set1 set2)
  (if (null? set1)
      set2
      (union-set (cdr set1) (adjoin (car set1) set2))))
(union-set '(a b c d e) '(a p q r e f))   ; (d c b a p q r e f)

; Sets as binary trees

(define (entry tree) (car tree))
(define (left-branch tree) (cadr tree))
(define (right-branch tree) (caddr tree))
(define (make-tree entry left right)
  (list entry left right))

(define (element-of-set? x set)
  (cond ((null? set) #f)
        ((= x (entry set)) #t)
        ((< x (entry set))
         (element-of-set? x (left-branch set)))
        ((> x (entry set))
         (element-of-set? x (right-branch set)))))

(define (adjoin-set x set)
  (cond ((null? set) (make-tree x '() '()))
        ((= x (entry set)) set)
        ((< x (entry set))
         (make-tree (entry set)
                    (adjoin-set x (left-branch set))
                    (right-branch set)))
        ((> x (entry set))
         (make-tree (entry set) (left-branch set)
                    (adjoin-set x (right-branch set))))))

; Balanced binary trees

; Exercice 2.63
(define (tree->list-1 tree)
  (if (null? tree)
      '()
      (append (tree->list-1 (left-branch tree))
              (cons (entry tree)
                    (tree->list-1
                     (right-branch tree))))))
(define (tree->list-2 tree)
  (define (copy-to-list tree result-list)
    (if (null? tree)
        result-list
        (copy-to-list (left-branch tree)
                      (cons (entry tree)
                            (copy-to-list
                             (right-branch tree)
                             result-list)))))
  (copy-to-list tree '()))

; Exercice 2.64
(define (list->tree elements)
  (car (partial-tree elements (length elements))))
(define (partial-tree elts n)
  (if (= n 0)
      (cons '() elts)
      (let ((left-size (quotient (- n 1) 2)))
        (let ((left-result
               (partial-tree elts left-size)))
          (let ((left-tree (car left-result))
                (non-left-elts (cdr left-result))
                (right-size (- n (+ left-size 1))))
            (let ((this-entry (car non-left-elts))
                  (right-result
                   (partial-tree
                    (cdr non-left-elts)
                    right-size)))
              (let ((right-tree (car right-result))
                    (remaining-elts
                     (cdr right-result)))
                (cons (make-tree this-entry
                                 left-tree
                                 right-tree)
                      remaining-elts))))))))



;; Exemple 2.3.4 -- arbres de Huffman

(define (make-leaf symbol weight) (list ('leaf symbol weight)))
(define (leaf? object) (eq? (car object) 'leaf))
(define (symbol-leaf x) (cadr x))
(define (weight-leaf x) (caddr x))

; Encodage
(define (make-code-tree left right)
  (list left
        right
        (append (symbols left) (symbols right))
        (+ (weight left) (weight right))))
(define (left-branch  tree) (car  tree))
(define (right-branch tree) (cadr tree))
(define (symbols tree)
  (if (leaf? tree)
      (list (symbol-leaf tree))
      (caddr tree)))
(define (weight tree)
  (if (leaf? tree)
      (weight-leaf tree)
      (cadddr tree)))

; D�codage
(define (decode bits tree)
  (define (decode-1 bits current-branch)
    (if (null? bits)
        '()
        (let ((next-branch
               (choose-branch (car bits) current-branch)))
          (if (leaf? next-branch)
              (cons (symbol-leaf next-branch)
                    (decode-1 (cdr bits) tree))
              (decode-1 (cdr bits) next-branch)))))
  (decode bits tree))
(define (choose-branch bit branch)
  (cond ((= bit 0) (left-branch branch))
        ((= bit 1) (right-branch branch))
        (else (error "bad bit: CHOOSE-BRANCH" bit))))

(define (adjoin-set x set)
  (cond ((null? set) (list x))
        ((< (weight x) (weight (car set))) (cons x set))
        (else (cons (car set) (adjoin-set x (cdr set))))))


;; �quivalent � range() en Python -- � compl�ter, plus g�n�rique que
;; "enumerate-interval" d�finit plus haut.

;; Lire / �crire fichier CSV

