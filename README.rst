Structure and Implementation of Computer Programs
=================================================

Il est possible de télécharger une version PDF depuis le dépôt SICP
sur `GitHub`_.

Gambit-C
========

On peut consulter toute la documentation sur le site principal de
`Gambit-C`_.
On peut trouver les sources de `Gambit-C sur GitHub`_.
On peut y trouver une `introduction à Scheme avec Gambit-C`_.

.. _`GitHub` : https://github.com/sarabander/sicp-pdf.git
.. _`Gambit-C sur GitHub` : https://github.com/feeley/gambit.git
.. _`Gambit-C` : http://gambitscheme.org
.. _`introduction à Scheme avec Gambit-C` : http://gambitscheme.org/wiki/images/a/a7/A_Tour_of_Scheme_in_Gambit.pdf
