#!/usr/bin/env svm

;; "The Scheme Programming Language, 4th edition"
;; http://www.scheme.com/tspl4

;; Continuations http://www.scheme.com/tspl4/further.html#./further:h3

(define call/cc call-with-current-continuation)
(newline)
(display (call/cc
          (lambda (k)
            (* 5 4))))
(newline)
(display (call/cc
          (lambda (k)
            (* 5 (k 4)))))
(newline)
(display (call/cc
          (lambda (k)
            (* 5 (k 4)))))
(newline)

(define product-ex
  (lambda (ls)
    (call/cc
     (lambda (break)
       (let f ((ls ls))
         (cond
          ((null? ls) 1)
          ((= (car ls) 0) (break 0))
          (else (* (car ls) (f (cdr ls))))))))))

(display (product-ex '(1 2 3 4 5)))
(newline)

(let ((x (call-with-current-continuation (lambda (k) k))))
  (x (lambda (ignore) "hi")))

(display (((call/cc (lambda (k) k)) (lambda (x) x)) "HEY!"))
(newline)

(define retry #f)
(define factorial
  (lambda (x)
    (if (= x 0)
        (call-with-current-continuation (lambda (k) (set! retry k) 1))
        (* x (factorial (- x 1))))))

(display (factorial 4))
(newline)


;; Continuations for various forms of multitasking

(define lwp-list '())
(define lwp
  (lambda (thunk)
    (set! lwp-list (append lwp-list (list thunk))))) 

(define start
  (lambda ()
    (let ([p (car lwp-list)])
      (set! lwp-list (cdr lwp-list))
      (p))))

(define pause
  (lambda ()
    (call/cc
     (lambda (k)
       (lwp (lambda () (k #f)))
       (start)))))

; The following light-weight processes cooperate to print an infinite
; sequence of lines containing "hey!".

(lwp (lambda () (let f () (pause) (display "h") (f))))
(lwp (lambda () (let f () (pause) (display "e") (f))))
(lwp (lambda () (let f () (pause) (display "y") (f))))
(lwp (lambda () (let f () (pause) (display "!") (f))))
(lwp (lambda () (let f () (pause) (newline) (f))))

(start)
